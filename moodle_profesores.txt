Estas indicaciones son muy telegráficas, si queréis nos vemos un día para que resulte más fácil.

Tenéis el perfil de creadores de cursos.

Si os matriculáis en el curso "Ejemplo con datos" podeis probar los exámenes que tiene.

Para importar preguntas de otros cursos por si os resultan útiles:
1. Activar edición
2. Administración > Preguntas
3. Importar > Formato GIFT > buscar este archivo de nombre tan breve:
cuestionario-tic_1_bach_ab-valor_por_defecto_para_miscel_nea-20160419-1804.txt
4. Marcarlo y pulsar en "Elegir"

El alumnado se puede matricular automáticamente con solo contar con un 
correo válido. Lo que no podéis hacer es cambiarle vosotros su 
contraseña. Para ello que los alumnos busquen en "participantes" al 
"Coordinador TIC" y que le soliciten una contraseña nueva.

Para escribir fórmulas matemáticas escribid el código LaTex entre $$ y $$:

$$ \sqrt 2 \cdot \x3$$


Tengo recogidas estas notas sobre qué tipo de cuestionarios usar (no sé si os serán muy útiles porque son muy breves):
Cuestionario:
    Ensayo: para pregunta que no corrige el ordenador, por tanto
    respuesta libre.  Emparejamiento: Al alumno se le muestran varias
    opciones de respuesta para que asocie cada una de ellas con la
    pregunta o elemento correspondiente

    Opción múltiple: Al alumno se le presentan varias posibilidades
    de respuesta para que elija entre ellas la, o las, que considere
    correcta.  Si solo hay una correcta el examen solo le permite escoger
    una. Si hay varias correctas hay que poner a las incorrectas una
    puntuación de -100 % porque sino contestando todas las respuestas
    (correctas e incorrectas) lo da por bueno. De esta manera no resta
    punto y si se eligen las correctas pero también una incorrecta la
    puntuación es 0.

    Respuestas anidadas (Cloze): El profesor debe introducir un texto,
    en un formato determinado, para definir si el alumno debe dar una
    respuesta corta, elegir de entre una serie de opciones o realizar un
    cálculo sencillo. Son más versátiles que el resto de las preguntas


    Verdadero/Falso: Pregunta con respuesta del tipo 'Verdadero' o
    'Falso'.

    respuesta corta: para escribir la respuesta.




Examen:


Desmarcar para abrirlo y para cerrarlo con fechas pasadas. Cuando ya
sepa la fecha del examen  o en la misma hora le cambio la fecha para que
lo hagan.  OJO: si no desmarco para abrir significa que está disponible
siempre.



barajar preguntas 
barajar dentro de las preguntas 
1 intento 
modo adaptativo no 
aplicar penalizaciones no

Revisar: Inmediatamente. Marcar solo respuestas, soluciones y puntuaciones
Más tarde, mientras el cuestionario está aún abierto y marcar solo Puntuaciones 
Después de cerrar el cuestionario y marcar solo Puntuaciones
así muestro las soluciones solo al acabar el examen, luego solo ven
la puntuación.

seguridad no funciona

para editar el cuestionario hay que pulsar en actualizar (vaya
traducción)

tener 20 preguntas y añadir 8 fijas y 4 aleatorias (el ordenador no
las repite) , por ejemplo, o las 10 aleatorias. (aunque eso puede hacer
que salgan todas las preguntas de una parte del tema) 20 preguntas se
tardan 5 minutos sabiendo las respuestas. Un alumno estimo que tardará
10 o 15 minutos.


Cuestionario con una única pregunta de ensayo: no merece la pena,
mejor imprimir y escribir en el papel.


Tarea "texto en línea" actividad en línea

Tarea "Subir un solo archivo", el fichero para hacer el examen hay que 
colgarlo en el foro en el momento de hacer el exámen, no he visto otra 
manera de hacerlo.

Tarea "subida avanzada de archivos" permite subir varios ficheros, el 
fichero para hacer el examen hay que colgarlo en el foro en el momento
de hacer el exámen, no he visto otra manera de hacerlo.


Calificación sobre 100 para disponer de decimales porque sobre 10 solo deja
enteros 
Poner fecha de apertura y cierre (ver más abajo, aunque tenga
puesta fecha el examen se ve, lo que se hace es ocultar, no obstante está
bien poner fecha porque así ya no lo pueden hacer después del cierre).
impedir envío retrasados
permitir reenvío: no 
alerta email prof:sí 
Comentario en línea: No
Después ocultarlo para que no lo vean y desocultarlo a la hora del examen.
NO hace falta ocultarlo porque aunque suban un fichero todavía no saben
qué se pregunta, y eso se pone en el momento en el foro.


Encuesta
En añadir actividad, no pulsamos "Encuesta", sino "Módulo de encuesta".
Elegimos preguntas de tipo: 
Opción múltiple (sólo una respuesta - lista desplegable)
porque es la más rápida de contestar y la encuesta ocupa menos pantalla.
Si queremos elegir más de una respuesta elegir: 
Elección múltiple (varias respuestas).
